// script.js
document.addEventListener("DOMContentLoaded", function() {
    // JavaScript code can go here if needed
});
// main.js
const { app, BrowserWindow, Menu } = require('electron');

function createWindow() {
    const win = new BrowserWindow({
        width: 800,
        height: 600,
        webPreferences: {
            nodeIntegration: true,
        },
    });

    win.loadFile('index.html');

    const menuTemplate = [
        {
            label: 'File',
            submenu: [
                { label: 'New' },
                { label: 'Open' },
                { label: 'Save' },
                { type: 'separator' },
                {
                    label: 'Exit',
                    click() { app.quit(); }
                },
            ],
        },
        {
            label: 'Edit',
            submenu: [
                { label: 'Undo' },
                { label: 'Redo' },
            ],
        },
    ];

    const menu = Menu.buildFromTemplate(menuTemplate);
    Menu.setApplicationMenu(menu);
}

app.whenReady().then(createWindow);

app.on('window-all-closed', () => {
    if (process.platform !== 'darwin') {
        app.quit();
    }
});

app.on('activate', () => {
    if (BrowserWindow.getAllWindows().length === 0) {
        createWindow();
    }
});
// script.js
let currentSlide = 0;
const slides = document.querySelectorAll('.slide');

function showSlide(index) {
    slides.forEach((slide, i) => {
        slide.classList.toggle('active', i === index);
    });
}

function nextSlide() {
    currentSlide = (currentSlide + 1) % slides.length;
    showSlide(currentSlide);
}

function prevSlide() {
    currentSlide = (currentSlide - 1 + slides.length) % slides.length;
    showSlide(currentSlide);
}

// Show the first slide initially
showSlide(currentSlide);
